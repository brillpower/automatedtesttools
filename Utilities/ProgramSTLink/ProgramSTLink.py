import time

import subprocess

print("*************************************************")
print("Confirm ST-LINK Successful Download Python Script")
print("*************************************************")


Attempts = 0
Passed = 0 

while Passed == 0 and Attempts < 10 : 
    Attempts += 1
    print ("Firmware Download Attempt " + str(Attempts) )
        
    ## call date command ##
    p = subprocess.Popen("sudo /home/pi/stlink/build/Release/bin/st-flash write ./Utilities/ReadEEPROM/EEPROMDump.bin 0x8000000", stdout=subprocess.PIPE, shell=True)

    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
    ## Interact with process: Send data to stdin. Read data from stdout and stderr, until end-of-file is reached.  ##
    ## Wait for process to terminate. The optional input argument should be a string to be sent to the child process, ##
    ## or None, if no data should be sent to the child.
    (output, err) = p.communicate()
     
    ## Wait for date to terminate. Get return returncode ##
    p_status = p.wait()
    print ("Command output : ", output)
    print ("Command exit status/return code : ", p_status)
    if p_status != 0:
        print (" ")
        print (" ")
        print ("Download Failed - Retrying")
        time.sleep(10)
        continue
    else :
        print("***SUCCESS*** ST-LINK Successful Download")
        f= open("DownloadPassed.Log","w+")
        f.write(str("Std Output \n"))
        f.write(str(output))
        f.write(str("Std Err \n"))
        f.write(str(err))
        f.close()
        

        break



