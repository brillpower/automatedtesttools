import serial, datetime,time

print("*********************************")
print("Monitor EEPROM Dump Python Script")
print("*********************************")

ser = serial.Serial('/dev/ttyUSB1' ,baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, xonxoff=False, rtscts=False,timeout=0.100)
ser.flushInput()
now = time.time()

AllBytes =[]

while True:
    later = time.time()
    difference = (later - now)
    #print (difference)
    if  int(difference) > 40:
        print("40s Passed")
        f= open("EEPROMDump.Log","w+")
        f.write(str(AllBytes))
        f.close()
        break
    try:
        ser_bytes = ser.readline()
        AllBytes += ser_bytes
        #decoded_bytes = float(ser_bytes[0:len(ser_bytes)-2].decode("utf-8"))
        if ser_bytes:
            print(ser_bytes)
    except:
        print("Keyboard Interrupt")
        break

