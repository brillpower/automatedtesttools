#!/bin/bash
export PYTHONUNBUFFERED=1

figlet -f slant "Test Automation"
echo " "


echo " "
figlet -f slant "Stage 1 - Dumping EEPROM"

python3 ./Utilities/ProgramSTLink/ProgramSTLink.py &
while [ ! -f ./DownloadPassed.Log ]
do
  echo "Pending for DownloadPassed.Log"
  sleep 10
done


python3 ./Utilities/ReadEEPROM/MonitorEEPROMDump.py &
while [ ! -f ./EEPROMDump.Log ]
do
  echo "Pending for EEPROMDump.Log"
  sleep 10
done


echo " Stage 1 - Over " 


figlet -f slant "Stage 2 - Programming Firmware"
echo " Needs Implementing " 
sleep 10
echo " Stage 2 - Over " 


figlet -f slant "Stage 3 - Testing Firmware"
echo " Needs Implementing " 
sleep 10
echo " Stage 3 - Over " 
