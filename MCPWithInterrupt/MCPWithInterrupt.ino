#include <SPI.h>
#include <mcp2515.h>
#include <CircularBuffer.h> //https://github.com/rlogiacco/CircularBuffer

#define SerialBaud 500000

#define PretendPrintedSensor_BASE_ID 256
#define NUM_SENSORS_EMULATE 24



volatile CircularBuffer< can_frame, 10> canBuffer;
volatile CircularBuffer< int, 10> canBufferCount;

MCP2515 mcp2515(10);

//                                    0          1           2            3              4             5         6          7         8             9                  10            11              12             13             14                 15    // Can be no larger!
const char *FunctionTable[15] = {"cfNull", "cfGetData", "cfSetData", "cfSetFlag", "cfClearFlag", "cfGetOK", "cfSetOK", "cfGetError", "cfSetError", "cfFunctionError", "cfIndexError", "cfSubIndexError", "cfDataLenError", "cfDataRangeError", "cfMiscError"};

const char *mbCanIndex[57] =
  //    0           1         2           3            4        5           6         7         8       9
{ "ciMasterTime", "ciState", "ciMaster", "ciModule", "ciBattery", "ciSystem", "ciTime", "ciEeprom", "ciLEDs", "ciSlotID",
  //           10              11              12             13            14       15         16       17        18       19
  "ciMtrVSystem", "ciMtrVSystemOut", "ciMtrVRipple", "ciMtrISystem", "ciModVBat", "ciModVOut", "ciModVL", "ciModIBat", "ciModIL", "ciModTBat",
  //          20        21         22        23         24            25       26          27          28           29
  "ciModTPwr", "ciModVUsb", "ciSerial", "ciCan", "ciModulePwm", "ciMasterPwm", "ciModICtrl", "ciModVCtrl", "ciModUsbPwm", "ciEeprom_1M",
  //              30           31            32       33            34       35          36            37             38          39
  "ciMstrModManager", "ciModuleOS", "ciMasterOS", "ciTrueIBat", "ciCalcSoc", "ciMstrICtrl", "ciMstrVCtrl", "ciModAcDuty", "ciModSocCtrl", "ciPwmLED",
  //         40     41        42           43        44         45        46          47         48             49              50             51         52        53          54            55         56
  "ciWifi", "ciRBat ", "ciBatStates", "ciModTQh", "ciModTQl", "ciFanPwm", "ciWifi2CAN", "ciVoutDiff", "ciOcvModel", "ciModSocCtrl2", "ciFirmwareUpgrade", "ciModbus", "ciSunspec", "ciExtCan", "ciPackStates", "ciNameplate", "ciSunspecSF"
};

typedef enum : unsigned int {cfNull, cfGetData, cfSetData, cfSetFlag, cfClearFlag, cfGetOK, cfSetOK, cfGetError, cfSetError, cfFunctionError, cfIndexError, cfSubIndexError, cfDataLenError, cfDataRangeError, cfMiscError, cfModuleInfo} mbCanFunction;


struct CAN_Breakout {
  unsigned int uiSubIndex : 8;
  unsigned int uiIndex : 8;
  unsigned int iID : 9;          // IID Does have 9 Bits.
  unsigned int Function : 4;
  unsigned int Unused : 1;
  unsigned int IDE_Bit : 1; // Can Remote / Can Data
  unsigned int RTR_Bit : 1; // CANExtended
};

union ByteAccess {
  uint32_t double_word;
  uint8_t octets[4];
  CAN_Breakout StatusFields;
};

struct can_frame frame;
volatile can_frame canMsg;
volatile int GlobalCount = 0;

void irqHandler() {
  noInterrupts();

  uint8_t irq = mcp2515.getInterrupts();
  if (irq & MCP2515::CANINTF_RX0IF) {
    if (mcp2515.readMessage(MCP2515::RXB0, &frame) == MCP2515::ERROR_OK) {
      // frame contains received from RXB0 message
      canBuffer.push(frame);
      GlobalCount++;
      canBufferCount.push(GlobalCount);

    }
  }

  if (irq & MCP2515::CANINTF_RX1IF) {
    if (mcp2515.readMessage(MCP2515::RXB1, &frame) == MCP2515::ERROR_OK) {
      // frame contains received from RXB1 message
      canBuffer.push(frame);
      GlobalCount++;
      canBufferCount.push(GlobalCount);

    }
  }

  interrupts();
}

void setup() {
  Serial.begin(SerialBaud);
  SPI.begin();
  mcp2515.reset();
  mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ);
  mcp2515.setNormalMode();
  pinMode(2, INPUT);
  attachInterrupt(0, irqHandler, FALLING);

  Serial.println("------- CAN Read ----------");
  Serial.println("ID  DLC   DATA");
}



unsigned long timeout = 0;

void loop() {
  can_frame PullQueue;

  //Start the Service Timer.
  timeout = micros();

  // If We have messages to process.
  if (canBuffer.isEmpty() == false)
  {
    // Start of Message Handle
    PullQueue = canBuffer.pop();
    int LocalCount = canBufferCount.pop();
    union ByteAccess ID_Field;

    ID_Field.double_word = (PullQueue.can_id) ;
    if ((ID_Field.StatusFields.iID >= PretendPrintedSensor_BASE_ID) && (ID_Field.StatusFields.iID < PretendPrintedSensor_BASE_ID + NUM_SENSORS_EMULATE))
    {
      EmulatePrintedSensor(PullQueue);
    }

    //For Now, We dont mind about the time budget.

#if 0
    // Find out how Good / Bad we have been on the Time Budget.
    unsigned long Lag = micros() - timeout;
    if (Lag > 1000)
    {
      Serial.print("Lag :");
      Serial.println(Lag, DEC);
    }
#endif
  }
}

void EmulatePrintedSensor(can_frame Msg)
{
  union ByteAccess Msg_View;
  Msg_View.double_word = (Msg.can_id) ;

  Serial.print("Replying As ");
  Serial.println(Msg_View.StatusFields.iID, DEC);

  if ( Msg_View.StatusFields.uiSubIndex == 0)
  {
    Serial.println("InstCurrent");
  }
  else if ( Msg_View.StatusFields.uiSubIndex == 1)
  {
    Serial.println("AvgCurrent");
  }
  else if ( Msg_View.StatusFields.uiSubIndex == 2)
  {
    Serial.println("Printed_Temperature");
    ReturnTemperatureFrame(Msg);
  }
  else
  {
    Serial.println("I don't know what this was");
  }
}

void ReturnTemperatureFrame(can_frame Msg)
{
  union ByteAccess Msg_View;
  Msg_View.double_word = (Msg.can_id) ;

  // Return Message :
  struct can_frame ReturnMsg;
  union ByteAccess ReturnMsg_View;

  // Make the Return Message 1:1 with the Incoming message.
  ReturnMsg_View.double_word =  Msg_View.double_word;

  // Now - Get the ID to return to from the orignating Message Data Field.
  //ReturnMsg_View.StatusFields.iID = Msg.data[0];

  ReturnMsg_View.StatusFields.iID = Msg_View.StatusFields.iID;

  // Populate its uiInded and Sub Indexs with the RI, and RCSI.

  ReturnMsg_View.StatusFields.uiIndex = Msg.data[2];
  ReturnMsg_View.StatusFields.uiSubIndex = Msg.data[3];

  //Announce its a CF Get OK .
  ReturnMsg_View.StatusFields.Function = cfGetOK;

  ReturnMsg.data[0] = 0xFF;
  ReturnMsg.data[1] = 0xFF;
  ReturnMsg.data[2] = 0xFF;
  ReturnMsg.data[3] = 0xFF;

  ReturnMsg.can_id = ReturnMsg_View.double_word;
  mcp2515.sendMessage(&ReturnMsg);

  Serial.println("EndReply");
}
