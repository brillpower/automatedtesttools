max_retry=5
counter=0
command="st-flash write Mstr5er.bin 0x8000000"
until $command
do
   sleep 1
   [[ counter -eq $max_retry ]] && echo "Failed!" && exit 1
   echo "Trying again. Try #$counter"
   ((counter++))
done